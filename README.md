# Nicholas "Nick" Beech  
**DevOps Engineer**
### Education:  
University of Maryland Baltimore County (UMBC) BS Information Systems  
### Certificates/Licenses

- Rancher Labs Certified Rancher Operator-Level One (May 2021) no expiration 
- SUSE Certified Administrator in SUSE Rancher 2. (August 10, 2021) no expiration 
- [HDP Developer Java - Hortonworks (January 22, 2018) no expiration](https://cc.sj-cdn.net/certificate/3az0o74wyqr6i/certificate-ch4zu3xed38n.pdf?response-content-disposition=attachment&Expires=1647629611&Signature=Nb3LXLQPHHuYLPKVL4rKs~c-WDPYWeT1kxjgR0S2AXGqtl19kqQiFzPbEbAtdbEF7FEZ4hau3uGTqxSkRPgKeYhiSIZX~4um6AUWWIvvXP5uYwi5QJVs6HoBHLJOUfxbCHzO2kVrxhlVwlNorN0zFr0w6TqreRomqGYacQ-TcBYCGYnZoAZ0UpK1QvWhYPKdrgLu393KRYP0jzSus0wYECQEKI4Caw2ckOTWGIuZhoZXaeZ6Cw4cTjUWy5C0R-MYQLyPF7j6fyEb0we0~jBbJGGJ~0WzDsO57uT2ZEez0mv2sm5-VbRsW26T2HcqspCJKbRAsdbB~Oxq5Ke8SYLcSw__&Key-Pair-Id=APKAI3B7HFD2VYJQK4MQ)

---
### Work Experience   
## *TekSystems*  
Remote
4/2022 - Present

**Senior DevOps Engineer**
- Automate AMI creation used for Azure DevOps Agents with Azure DevOps Pipelines, Ansible, and Packer
- Create/update terraform modules
- Create Azure DevOps Pipelines for terraform deployments
- Serves as Scrum Master for team


## *Intelliforce-IT Solutions Group*  
Ellicott City, MD  
11/23/2015 - 3/31/2022  
### Roles:

**Software Engineer (DevOps) March 2020 - March 2022**  
- Automated AMI (Amazon Machine Image) creation through the use of Packer, Ansible, and GitLab CI/CD Pipelines
- Created Ansible playbooks to facilitate automated installation of require software
- Created Packer (HashiCorp Packer) scripts (JSON & HCL) to facilitate automated installation
- Utilized Terraform to stand up multiple Kubernetes Clusters in multiple environments (AWS, OpenStack)
- Automated Helm deployment with FluxCD
- Promoted to Senior DevOps Engineer
- Sucessfully delivered a large scale Kubernetes cluster in a short period of time (1 week)

**Software Engineer May 2017 - March 2020**  
- Wrote, maintained, and updated Java MapReduce code for a large-scale data processing project
- Generated Unit Tests for Java MapReduce code
- Created and maintained 40 - 60 development VM's
- Studied, developed, and implemented SonarQube (Code Quality Management) as well as created new SonarQube Rules
- Provided technical support to developers VM's through ssh, linux, and bash scripting

**Software Engineer November 2015 - May 2017**  
- Deployed and maintained Java MapReduce analytics for a large production platform
- Utilized Hadoo and HDFS to verify analytic output requirements
- Troubleshot issues with Java MapReduce analytics through reseaerch and discovery
- Modified XML workflows to perform requested tasks
- Documented processses for standardization and future modification as well as process improvements


## *Accenture Federal Services*  
Annapolis Junction, MD  
5/5/2014 - 11/21/2015  

### **Senior System Developer Analyst**  
- Planned, developed, documented, and executed enterprise level configuration management programs
- Articulated provisions for configuration identification, change control, configuration status accounting, and configuration audits
- Worked collaboratively to ensure software changes are properly validated and approved prior to incorporating
- Established and implement requirement management
- Created numerous process flows for senior management and customers
- Served as the resident requirement expert
- Developed and implemented new methodologies for training requirement writing

## *Wawa Food Markets, Inc.*  
Gambrills, MD  
6/2012 - 5/4/2014  
### **Customer Service Assocaite/Shift Manager**  
- Provided exemplary customer service, lauded several times by management for going above and beyond.
- Trained and managed new Customer Service Associates  

## *AllWorld Language Consultants Inc.*  
Bethesda, MD  
8/2008 - 6/2010  
### **Geospatial Analyst**  
- Provided authoritative responses to questions concerning the proper Romanization and correct usage of names in the Middle East
- Populated and assisted in the maintenance of the Geographic Names Database (GNdB)
- Performed regular SQL queries daily to verify status of the entries in GNdB
- Provided transliteration of names from native source maps to English in accordance with standards set forth by the US BGN

## *C Company 741st Military Intelligence Battalion*  
Fort George G. Meade, MD  
5/2006 - 8/2008  
### **Senior Cryptologic Linguist**  
- Served as Senior Language Advisor for Arabic Linguists within the Company
- Created and executed lesson plans for Arabic language training at the company level
- Performed day to day systems management and support to include account management, equipment installation and removal, user training and requests for fixes, repairs/replacements and upgrades  
- Built and maintained 3 work related web domains in addition to daily duties
- Served as the Web Administrator for Company website and provided on-site support to users experiencing issues

## *B Company 1-3 Brigade Troops Battalion*  
Fort Stewart, GA  
5/2004 - 5/2006
### **Cryptologic Linguist**  
- Assisted in the assembly and maintenance of Windows based computer networks while deployed to a hostile environment
- Developed and tested a data repository and user-interface for linguist translations (Visual Basic/Access)
- Worked closely with team analysts to establish Standard Operating Procedures
- Performed maintenance and upkeep on Windows Severs, including installing patches and security updates

## *D Company 103rd Military Intelligence Battalion*
Fort Stewart, GA  
4/2002 - 5/2004  
### **Cryptologic Linguist**  
- Provided live face-to-face two-way interpretation and translation during briefs and interviews
- Served as an interpreter with the Judge Advocate General team to aide in the re-establishment of the Judicial system in Al-Fallujah, Iraq
- Evaluated media of all formats and prioritized it based on value in providing force protection 


